FactoryBot.define do
  factory :micropost do
    association :user
    content { "hogehoge" }
    picture {Rack::Test::UploadedFile.new(File.join(Rails.root,'spec/fixtures/for_test.png'))}
    created_at { Time.zone.now }
  end

  factory :micropost2, class: Micropost do
    association :user
    content { "I just ate an orange!" }
    created_at { 10.minutes.ago }
  end

 	factory :micropost3, class: Micropost do
    association :user
  	content {"Check out the @tauday site by @mhartl: http://tauday.com"}
  	created_at { 3.years.ago }
  end

	factory :micropost4, class: Micropost do
    association :user
  	content { "Sad cats are sad: http://youtu.be/PKffm2uI4dk" }
    created_at { 2.hours.ago } 
	end

end
