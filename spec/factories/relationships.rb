FactoryBot.define do
	factory :test_relationship, class:Relationship do
		followed_id { 1 }
		follower_id { 2 }
	end 
end
