FactoryBot.define do
  factory :user do
    name { "hoge" }
    sequence(:email) {|n| "hoge#{n}@yahoo.co.jp" }
    password {"password"}
    password_confirmation {"password"}
    admin { true }
    activated { true }
    activated_at { Time.zone.now }

    trait :with_microposts do
      after(:create) do |user|
        FactoryBot.create(:micropost,user: user)
        FactoryBot.create(:micropost2,user: user)
        FactoryBot.create(:micropost3,user: user)
        FactoryBot.create(:micropost4,user: user)      
      end
    end
  end

  factory :other_user ,class: User do
  	name { "piyo" }
  	sequence(:email) {|n| "piyo#{n}@yahoo.co.jp" }
  	password {"password"}
  	password_confirmation {"password"}
    admin { false }
    activated { true }
    activated_at { Time.zone.now }

    trait :with_microposts do
      after(:create) do |other_user|
        FactoryBot.create(:micropost,user: other_user)
        FactoryBot.create(:micropost2,user: other_user)
        FactoryBot.create(:micropost3,user: other_user)
        FactoryBot.create(:micropost4,user: other_user)
      end      
    end
  end

  factory :admin_user ,class: User do
    name { "admin" }
    sequence(:email) {|n| "admin#{n}@yahoo.co.jp" }
    password {"password"}
    password_confirmation {"password"}
    admin { true }
    activated { true }
    activated_at { Time.zone.now }
  end

  factory :non_admin_user ,class: User do
    name { "non_admin" }
    sequence(:email) {|n| "non_admin#{n}@yahoo.co.jp" }
    password {"password"}
    password_confirmation {"password"}
    admin { false }
    activated { true }
    activated_at { Time.zone.now }
  end

  factory :non_activated_user ,class: User do
    name { "non_activated" }
    sequence(:email) {|n| "non_activated#{n}@yahoo.co.jp" }
    password {"password"}
    password_confirmation {"password"}
    admin { false }
    activated { false }
  end

# other_userをフォローしているuserのFactory
  factory :active_relationship_user, class: User do
    name { "active_relationship_user" }
    sequence(:email) {|n| "active_relation#{n}@yahoo.co.jp" }
    password {"password"}
    password_confirmation {"password"}
    admin { true }
    activated { true }
    activated_at { Time.zone.now }

    trait :with_microposts do
      after(:create) do |active_relationship_user|
        FactoryBot.create(:micropost,user: active_relationship_user)
        FactoryBot.create(:micropost2,user: active_relationship_user)
        FactoryBot.create(:micropost3,user: active_relationship_user)
        FactoryBot.create(:micropost4,user: active_relationship_user)      
      end
    end

    after(:create) do |user|
       other_user = FactoryBot.create(:other_user, :with_microposts)
       FactoryBot.create(:test_relationship, follower_id: user.id,followed_id: other_user.id)
    end
  end

# other_userからフォローされているuserのFactory
  factory :passive_relationship_user, class: User do
    name { "passive_relationship_user" }
    sequence(:email) {|n| "passive_relation#{n}@yahoo.co.jp" }
    password {"password"}
    password_confirmation {"password"}
    admin { true }
    activated { true }
    activated_at { Time.zone.now }

    trait :with_microposts do 
      after(:create) do |passive_relationship_user|
        FactoryBot.create(:micropost,user: passive_relationship_user)
        FactoryBot.create(:micropost2,user: passive_relationship_user)
        FactoryBot.create(:micropost3,user: passive_relationship_user)
        FactoryBot.create(:micropost4,user: passive_relationship_user)      
      end
    end

    after(:create) do |user|
      other_user = FactoryBot.create(:other_user, :with_microposts)
      FactoryBot.create(:test_relationship, follower_id: other_user.id,followed_id: user.id)
    end
  end
end
