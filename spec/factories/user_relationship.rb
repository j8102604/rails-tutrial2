FactoryBot.define do
	factory :active_relationship do
		user
		follower_id
	end

	factory :passive_relationship do
		user
		followed_id
	end
end