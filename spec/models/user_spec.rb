require 'rails_helper'

RSpec.describe User, type: :model do
	describe "Invalid入力の検証(バリデーション)" do
		let(:non_activated_user) { FactoryBot.create(:non_activated_user)}
		let(:user) { FactoryBot.create(:user)}

		before do
			micropost = FactoryBot.create(:micropost, user: user)
		end

		it "authenticated? should return false for a user with nil digest" do
			expect(non_activated_user.authenticated?(:remember,'')).to be_falsey
		end

		it "should valid" do
	  	expect(non_activated_user).to be_valid
		end

		it "name should be present" do
			non_activated_user.name = ""
			expect(non_activated_user).not_to be_valid
			expect(non_activated_user.errors[:name].size).to be >= 1
		end

		it "email should be present" do
			non_activated_user.email = ""
			expect(non_activated_user).not_to be_valid
			expect(non_activated_user.errors[:email].size).to be >= 1
		end

		it "name should not be too long" do
			non_activated_user.name = "a"*51
			expect(non_activated_user).not_to be_valid
			expect(non_activated_user.errors[:name].size).to be >= 1
		end

		it "email should not be too long" do
			non_activated_user.email = "a"*244 + "@example.com"
			expect(non_activated_user).not_to be_valid
			expect(non_activated_user.errors[:email].size).to be >= 1
		end

		it "email validation should accept valid addresses" do
			invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.foo@bar_baz.com foo@bar+baz.com]
			invalid_addresses.each do |invalid_address|
				non_activated_user.email = invalid_address
				expect(non_activated_user).not_to be_valid,"#{invalid_address.inspect} should be valid"
			end
		end

		it "email addresses should be unique" do
			duplicate_user = non_activated_user.dup
			duplicate_user.email = non_activated_user.email.upcase
			non_activated_user.save
			expect(duplicate_user).not_to be_valid
		end

		it "password should be present (noblank)" do
			non_activated_user.password = non_activated_user.password_confirmation = ""
			expect(non_activated_user).not_to be_valid
		end

		it "password should hava a minimum length" do
			non_activated_user.password = non_activated_user.password_confirmation = "a"*5
			expect(non_activated_user).not_to be_valid
		end

		it "associated microposts should be destroyed" do
			expect{ user.destroy }.to change{ Micropost.count }.by(-1)
		end
	end

	describe "フォロー/フォロワーの関係" do
		context "アクティベートされたユーザー間でフォロー/アンフォローを行うと" do
			let(:user) { FactoryBot.create(:user) }
			let(:other_user){ FactoryBot.create(:other_user) }

			it "relastionshipのDBが正しく更新される" do
				expect(user.following?(other_user)).to eq false
				user.follow(other_user)
				expect(user.following?(other_user)).to eq true
				expect(other_user.followers.include?(user)).to eq true
				user.unfollow(other_user)
				expect(user.following?(other_user)).to eq false
			end
		end
	end

	describe "ステータスフィールドのテスト" do
		# context "フォローしているユーザーの投稿を確認すると" do
		# 	let(:active_relationship_user) { FactoryBot.create(:active_relationship_user, :with_microposts) }

		# 	it "表示されている" do
		# 		other_user.microposts.each do |post_following|
		# 			expect(active_relationship_user.feed.include?(post_following)).to eq true
		# 		end
		# 	end
		# end

		context "自分自身の投稿を確認すると" do
			let(:user) { FactoryBot.create(:user, :with_microposts)}

			it "表示されている" do
				user.microposts.each do |post_self|
					expect(user.feed.include?(post_self)).to eq true
				end
			end
		end

		context "フォローしていないユーザーの投稿を確認すると" do
			let(:user) { FactoryBot.create(:user) }
			let(:other_user) { FactoryBot.create(:other_user, :with_microposts) }

			it "表示されていない" do
				other_user.microposts.each do |post_unfollowd|
					expect(user.feed.include?(post_unfollowd)).to eq false
				end
			end
		end


	end
end
