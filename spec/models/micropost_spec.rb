require 'rails_helper'

RSpec.describe Micropost, type: :model do
	let(:most_recent_post) { FactoryBot.create(:micropost) }
	let(:micropost) { FactoryBot.create(:micropost2) }

	context "validation test" do
		before do
			FactoryBot.create(:micropost3)
			FactoryBot.create(:micropost4)
		end

		it "should be valid" do
			expect(micropost).to be_valid
		end

		it "user id should be present" do
			micropost.user_id = nil
			expect(micropost).not_to be_valid
		end

		it "content should be presnet" do
			micropost.content = " "
			expect(micropost).not_to be_valid
		end

		it "content should be at most 140 characters" do
			micropost.content = "a"*141
			expect(micropost).not_to be_valid
		end

		it "order should be most recent first" do
			expect(most_recent_post).to eq Micropost.first
		end
	end
end
