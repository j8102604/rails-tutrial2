require 'rails_helper'

RSpec.describe "sessions/new.html.slim", type: :view do
  let(:user) { FactoryBot.create(:user) }

  context "ログイン用のパスを開き、有効な情報でログインすると" do
    before do
      visit login_path
      fill_in 'email', with: user.email
      fill_in 'password', with: user.password
      click_on 'submit'
    end

    it "ログイン用リンクが消える" do
      expect(page).not_to have_link 'Log in'
    end

    it "ログアウト用リンクが表示される" do
      click_on 'Account'
      expect(page).to have_link 'Log out'
    end

    it "プロフィール用リンクが表示される" do
      click_on 'Account'
      expect(page).to have_link 'Profile'
    end
    
    context "その後、ログアウト用のリンクをクリックすると" do
      before do
        click_on 'Account'
        click_on 'Log out'      
      end

      it "ログイン用リンクが表示される" do
        expect(page).to have_link 'Log in'
      end

      it "アカウント用リンクが消える" do
        expect(page).not_to have_link 'Account'
      end
    end
  end

  context 'ユーザー編集画面にアクセス後、ログインすると' do
    before do
      visit edit_user_path(user)
      fill_in 'email', with: user.email
      fill_in 'password', with: user.password
      click_on 'submit'
    end

    it 'ログイン後、ユーザー編集画面が表示される' do
      expect(page).to have_title('Edit user')          
    end
  end
end
