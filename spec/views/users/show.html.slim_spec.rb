require 'rails_helper'

RSpec.describe "users/show.html.slim", type: :view do
	include ApplicationHelper

	context "投稿が30以上あるユーザーのプロフィールを表示すると" do
		let(:admin_user) { FactoryBot.create(:user) }

		before do
			80.times do
				post = FactoryBot.create(:micropost, user: admin_user)
			end
			visit login_path
      fill_in 'email', with: admin_user.email
      fill_in 'password', with: admin_user.password
      click_on 'submit'
 		end

		it "ユーザー情報がよしなに表示される" do
			expect(page).to have_title(full_title(admin_user.name))
			expect(page).to have_css('h1',text: admin_user.name)
			expect(page).to have_css('h1>img.gravatar')
		end

		it "ページネーション付きで投稿が表示される" do
			expect(page.body).to match admin_user.microposts.count.to_s
			expect(page).to have_css('div.pagination') , count: 1
			admin_user.microposts.paginate(page: 1).each do |micropost|
				expect(page.body).to match micropost.content
			end
		end
	end
end