require 'rails_helper'

RSpec.describe "users/index.html.slim", type: :view do
	context "30人以上のユーザー登録がある状態でユーザー一覧を見ると" do
		before do
			admin_user = FactoryBot.create(:user)
			80.times do
				other_user = FactoryBot.create(:other_user)
			end
			visit login_path
      fill_in 'email', with: admin_user.email
      fill_in 'password', with: admin_user.password
      click_on 'submit'
			visit users_path
		end

		it "ページネーションが表示される(対象がadmin以外の場合はdeleteリンクも同時に表示される)" do
			expect(page).to have_css('div.pagination'), count: 2
			User.paginate(page: 1).each do |user|
				expect(page).to have_link user.name, href: user_path(user)
				if !user.admin?
					expect(page).to have_link 'delete', href: user_path(user)
				end
			end
		end
	end

	context "admin以外のユーザーでログインすると" do
		before do
			other_user = FactoryBot.create(:other_user)
			visit login_path
			fill_in 'email', with: other_user.email
			fill_in 'password', with: other_user.password
			click_on 'submit'
			visit users_path
		end

		it "deleteのリンクが表示されない" do
			expect(page).not_to have_selector 'a', text: 'delete'
		end
	end
end