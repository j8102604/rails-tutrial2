require 'rails_helper'

RSpec.describe "誤ったユーザー登録", type: :view do
	before do
		visit signup_path
		fill_in 'name', with: 'test'
		fill_in 'email', with: 'test@example'
		fill_in 'password', with: '123'
		fill_in 'password_confirmation', with: '123'
		click_on 'submit'
	end
	
	it "hava error messages" do
		expect(page).to have_css('div',id: 'error_explanation')
		expect(page).to have_content('error')
	end
end

RSpec.describe "正しいユーザー登録" , type: :view do
	before do
		visit signup_path
		fill_in 'name', with: 'test'
		fill_in 'email', with: 'test@example.com'
		fill_in 'password', with: '123456'
		fill_in 'password_confirmation', with: '123456'
		click_on 'submit'
	end

	it "have flash message" do
		expect(page).to have_css('div',class: 'alert-info')
		expect(page).to have_content('Please check your email to activate your account')
	end
end