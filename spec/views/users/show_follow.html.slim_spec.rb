require 'rails_helper'

RSpec.describe "users/show_follow.html.slim", type: :view do
	let(:active_relationship_user) { FactoryBot.create(:active_relationship_user)}
	let(:passive_relationship_user) { FactoryBot.create(:passive_relationship_user)}

	it "following page" do
		visit following_user_path(active_relationship_user)
		fill_in 'email', with:active_relationship_user.email
		fill_in 'password', with:active_relationship_user.password
		click_on 'submit'
		expect(active_relationship_user.following.empty?).to eq false
		expect(page.body).to match active_relationship_user.following.count.to_s
		active_relationship_user.following.each do |user|
			expect(page.body).to have_link(user.name, href:user_path(user))
		end
	end

	it "followers page" do
		visit followers_user_path(passive_relationship_user)
		fill_in 'email', with:passive_relationship_user.email
		fill_in 'password', with:passive_relationship_user.password
		click_on 'submit'
		expect(passive_relationship_user.followers.empty?).to eq false
		expect(page.body).to match passive_relationship_user.followers.count.to_s
		passive_relationship_user.followers.each do |user|
			expect(page.body).to have_link(user.name, href:user_path(user))
		end		
	end
end