require 'rails_helper'

RSpec.describe "microposts_interface_test",type: :feature do
	let(:user) { FactoryBot.create(:user) }
	let(:other_user) { FactoryBot.create(:other_user)}

	describe "micropost interface" do
		before do
	    visit login_path
	    fill_in 'email', with: user.email
	    fill_in 'password', with: user.password
	    click_on 'submit'
		end

		context "when invalid message posted" do
			before do
				visit root_path
			end

			it "show error explanation" do
				expect{
					fill_in 'content', with: ''
					click_on 'submit'
				}.not_to change{ Micropost.count }
				expect(page).to have_css('div#error_explanation')
			end
		end

		context "when valid message posted" do
			before do
				visit root_path
			end

			it "Micropost.count up etc." do
				content = 'test'
				expect{
					fill_in 'content', with: content
					click_on 'submit'
				}.to change{Micropost.count}.by(1)
				expect(page.title).to eq full_title "Home"
				expect(page.body).to match content
			end
		end

		context "when delete a message" do
			before do
				visit root_path
				fill_in 'content', with: 'test'
				click_on 'submit'
			end

			it "Micropost.count down etc" do
				expect(page).to have_css('a',text: 'delete')
				expect{
					accept_alert do
						all('ol li')[0].click_on 'delete'
					end
					visit root_path
				}.to change{Micropost.count}.by(-1)
			end
		end

		context "visit other user's profile page" do
			before do
				visit root_path
				fill_in 'content', with: 'test'
				click_on 'submit'

				visit login_path
	    	fill_in 'email', with: other_user.email
	    	fill_in 'password', with: other_user.password
	    	click_on 'submit'
				
				visit user_path(user)
			end

			it "can't see the delete link" do
				expect(page).not_to have_css('a',text: 'delete')
			end
		end
	end

	describe "micropost sidebar count" do
		context "posted user" do
			before do
				visit login_path
		    fill_in 'email', with: user.email
		    fill_in 'password', with: user.password
		    click_on 'submit'

		    visit root_path
				fill_in 'content', with: 'test'
				click_on 'submit'

				visit root_path
			end

			it "matches count of microposts" do
				expect(page.body).to match "#{user.microposts.count} micropost"
			end
		end

		context "not posted user" do
			before do
				visit login_path
		    fill_in 'email', with: other_user.email
		    fill_in 'password', with: other_user.password
		    click_on 'submit'

				visit root_path
			end

			it "matches o microposts" do
				expect(page.body).to match "0 microposts"
				other_user.microposts.create!(content: "A micropost")
				visit root_path

				expect(page.body).to match "#{other_user.microposts.count} micropost"
			end
		end
	end
end