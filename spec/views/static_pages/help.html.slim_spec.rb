require 'rails_helper'

RSpec.describe "static_pages/help.html.slim", type: :view do
	context 'helpにアクセスすると' do
		before do
			visit help_path
		end

		it '正常にhelpページを表示する' do
			expect(current_path).to eq help_path
		end
	end 
end
