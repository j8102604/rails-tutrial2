require 'rails_helper'

RSpec.describe "static_pages/home.html.slim", type: :view do
	context 'homeにアクセスすると' do
		before do
			visit home_path
		end

		it '正常にhomeページを表示する' do
			expect(current_path).to eq home_path
		end

		it '適切なリンクが埋め込まれている' do
			expect(page).to have_link "sample app",href:root_path
			expect(page).to have_link "Home",href:root_path
			expect(page).to have_link "Help",href:help_path
			expect(page).to have_link "About",href:about_path
			expect(page).to have_link "Contact",href:contact_path
			expect(page).to have_link "Sign up now!",href:signup_path
		end
	end
end