require 'rails_helper'

RSpec.describe "static_pages/about.html.slim", type: :view do
	context 'aboutにアクセスすると' do
		before do
			visit about_path
		end

		it '正常にaboutページを表示する' do
			expect(current_path).to eq about_path
		end
	end 
end
