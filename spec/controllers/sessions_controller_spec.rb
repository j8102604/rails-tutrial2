require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  include SessionsHelper
  render_views

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end


  describe "rememberのテスト" do
    let(:user) {FactoryBot.create(:user)}

    context "rememberを使ってログインした場合" do
      before do
        log_in_as2(user,remember_me: '1')
      end

      it "cookies['remember_token']が空ではない" do
        expect(cookies[:remember_token]).not_to eq nil
      end
    end

    context "rememberを使用せずにログインした場合" do
      before do
        log_in_as2(user,remember_me: '1')
        delete :destroy
        log_in_as2(user,remember_me: '0')
      end

      it "cookies['remember_token']が空" do
        expect(cookies[:remember_token]).to eq nil
      end
    end

    context "あるユーザーをrememberメソッドで記憶すると、" do
      before do
        remember(user)
      end

      it "それがcurrent_userと同一になる" do
        expect(user).to eq current_user
        expect(logged_in?).to be true
      end
    end
  end

  describe "POST #create" do

    context "誤ったemailとパスワードでログインすると" do
      before do
        post :create, params: { session: { email:'Invalid@example.org', password:'Invalid'}}
      end

    	it "新しいセッションのフォームが再表示され、フラッシュが表示される" do
    		expect(response).to render_template("new")
        expect(flash[:danger]).to be_present
    	end
    end
  end

  describe "DELETE #destroy" do
    let(:user) {FactoryBot.create(:user)}

    context "2度目のログアウトをすると" do
      before do
        post :create, params: { session: { email:user.email, password:user.password }}

        delete :destroy
        delete :destroy
      end

      it "ルート画面にリダイレクトされる"do
        expect(response).to redirect_to(root_path)
      end
    end
  end
  # describe "フラッシュの表示後、別のページに移動すると" do

		# before do
  #     post :create, params: { session: { email:'Invalid@example.org', password:'Invalid'}}
  #     get help_path
  # 	end

  # 	it "フラッシュが消える" do
  #     expect(flash[:danger]).not_to be_present
  # 	end
  # end
end
