require 'rails_helper'

RSpec.describe AccountActivationsController, type: :controller do
	include SessionsHelper
	
	describe "account activation" do
		render_views
		let(:non_activated_user) { FactoryBot.create(:non_activated_user)}

		context "有効化トークンが不正な場合" do
			before do
				get :edit ,params:{id:"invalid token",email: non_activated_user.email}
			end

			it "ログインできない" do
				expect(logged_in?).to eq false
				expect(flash[:danger]).to be_present
				expect(response).to redirect_to root_url  
			end
		end

		context "トークンは正しいがメールアドレスが不正な場合" do
			before do
				get :edit ,params:{id:non_activated_user.activation_token,email:"invalid_email"}
			end

			it "ログインできない" do
				expect(logged_in?).to eq false
				expect(flash[:danger]).to be_present
				expect(response).to redirect_to root_url
			end
		end

		context "トークン、メールアドレス共に正しい場合" do
			before do
				get :edit ,params:{id:non_activated_user.activation_token,email: non_activated_user.email}
			end

			it "ログインできる" do
			  expect(logged_in?).to eq true
			  activated_user = assigns(:user)
			  expect(activated_user.activated).to eq true
				expect(flash[:success]).to be_present
				expect(response).to redirect_to user_url(id:activated_user.id)
			end
		end
	end
end
