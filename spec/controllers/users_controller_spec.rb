require 'rails_helper'

RSpec.describe UsersController, type: :controller do
	include SessionsHelper
	describe "post #create OK" do
		context "#create OK" do
			before do
				ActionMailer::Base.deliveries.clear
			end
			
			it "count up" do
				expect{ post :create , params:{user:{ name: "hoge",
																							email: "hoge@gmail.com",
																							password: "password",
																							password_confirmation: "password"}}
							}.to change{ User.count}.by(1)
			end

			it "redirect to root" do
				post :create , params:{user:{name:"test1",
																email:"test1@example.org",
																password:"123456",
																password_confirmation:"123456"}}

				expect(response).to redirect_to root_url
			end

			# it "メールを一通発行している" do
			# 	expect{
			# 		post :create , params:{user:{name:"test1",
			# 													email:"test1@example.org",
			# 													password:"123456",
			# 													password_confirmation:"123456"}}
			# 	}.to change{ActionMailer::Base.deliveries.count}.by(1)
			# end

			it "有効化されていない" do
				user = FactoryBot.create(:non_activated_user)
				expect(user.activated?).to eq false
			end
		end

		context "#create NG" do
			it "not_to count up" do
				expect{
					post :create , params:{user:{name:"test",
																			email:"test@example",
																			password:"123",
																			password_confirmation:"123"}}
				}.not_to change{ User.count }
			end

			it "render to Signup" do
				post :create , params:{user:{name:"test",
																			email:"test@example",
																			password:"123",
																			password_confirmation:"123"}}

				expect(response).to render_template :new
			end
		end
	end

 	describe "patch #update" do
 		let(:user) { FactoryBot.create(:user) }

 		context "NG" do
 			before do
	      log_in_as(user)
	 			patch :update, params:{id: user.id , user: {name:"",
	 																								email:"foo@invalid",
	 																								password:"foo",
	 																								password_confirmation:"bar"}}
 			end

	 		it "render to edit" do
	 			expect(response).to render_template :edit
	 		end

	 		context "without login" do
	 			before do
	 				log_out
	 				patch :update ,params:{id: user.id, user: {name: "Foo Bar",
																										email: "foo@bar.com"}}

	 			end

		 		it "redirect to login_url with flash" do
					expect(flash[:danger]).to be_present
					expect(response).to redirect_to login_url
	 			end
 			end

 			context "update admin status by other user" do
 				before do
	 				non_admin_user = FactoryBot.create(:non_admin_user)
	 				log_in_as(non_admin_user)
	 				patch :update ,params:{id: non_admin_user.id, user:{password: "password",
	 																												password_confirmation: "password",
	 																												admin: true}}
	 				non_admin_user.reload
	 			end

	 			it "will fail" do
	 				non_admin_user = assigns(:user)
	 				expect(non_admin_user.admin?).to be false
 				end
 			end

 			context "login with other user" do
 				before do
 					log_out
 					other_user = FactoryBot.create(:other_user)
	 				log_in_as(other_user)
	 				patch :update, params:{id: user.id, user:{name: "invalid edit", email: "hoge@example.com"}}
 				end
 				
 				it "redirect to root_url without flash" do
	 				expect(flash[:success]).not_to be_present
	 				expect(response).to redirect_to root_url
 				end
 			end
 		end

 		context "OK" do
			name = "Foo Bar"
			email = "foo@bar.com"

 			before do
 				log_in_as(user)
	 			patch :update, params:{id: user.id, user: {name: name,
																					email: email,
																					password: "",
																					password_confirmation: ""}}
				user.reload
 			end

 			it "flash not empty etc." do
 				updated_user = assigns(:user)
 				expect(flash[:success]).to be_present
 				expect(response).to redirect_to(updated_user)
 			end

 			it "user status updated" do
 				updated_user = assigns(:user)
	 			expect(updated_user.name).to eq name
	 			expect(updated_user.email).to eq email
 			end
 		end
 	end

 	describe "delete #destroy" do
 		let(:user) { FactoryBot.create(:user) }

 		context "non-admin user" do
 			let(:non_admin_user) { FactoryBot.create(:non_admin_user)}
 			before do
		 		log_in_as(non_admin_user)
 			end

	 		it "cant delete and redirect to root" do
 				expect {
 					delete :destroy, params:{id: non_admin_user.id}
 				}.not_to change{ User.count }
 				expect(response).to redirect_to root_url
		 	end
 		end

 		context "not logged in user" do	 		
	 		it "cant delete and redirect to login" do
	 			other_user = FactoryBot.create(:other_user)

 				expect {
 					delete :destroy, params:{id: other_user.id}
 				}.not_to change{ User.count }
 				expect(response).to redirect_to login_url
			end
 		end
 	end

 	describe "GET #edit" do
 		let(:user){ FactoryBot.create(:user) }

 		context "without login" do
	 		before do
 				get :edit, params:{id: user.id}
	 		end

	 		it "redirect to login_url with flash" do
 				expect(flash[:danger]).to be_present
 				expect(response).to redirect_to login_url
	 		end
 		end

 		context "login with other user" do
 			before do
 				other_user = FactoryBot.create(:other_user)
 				log_in_as(other_user)
 			end

 			it "redirect to root_url without flash" do
 				get :edit, params: {id: user.id}
 				expect(flash[:success]).not_to be_present
 				expect(response).to redirect_to root_url
 			end
		end
 	end

 	describe "GET #show" do
 		context "without login" do
 			let(:user) { FactoryBot.create(:user)}
 			let(:other_user) { FactoryBot.create(:other_user) }

 			it "should redirect following" do
 				get :following, params:{id:user.id}
 				expect(response).to redirect_to login_url
 			end

 			it "should redirect followers" do
 				get :followers, params:{id:user.id}
 				expect(response).to redirect_to login_url
 			end
 		end
 	end

 	describe "GET #index" do
 		before do
			get :index
 		end
 
		it "redirect to login_url" do
			expect(response).to redirect_to login_url
		end
 	end

  describe "GET #new" do
  	render_views

  	before do
  		get :new
  	end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "title is Sign up" do
    	expect(response.body).to have_css('div',text:'Sign up')
    end
  end
end
