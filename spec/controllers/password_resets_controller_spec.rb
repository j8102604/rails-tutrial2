require 'rails_helper'

RSpec.describe PasswordResetsController, type: :controller do
  include SessionsHelper

  describe "GET #new" do
    let(:user) { FactoryBot.create(:user)}

    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #creaate" do
    let(:user) { FactoryBot.create(:user)}

    context "post invalid email" do
      before do
        post :create, params:{password_reset:{email:""}}
      end

      it "render new template etc." do
        expect(flash.now[:danger]).to be_present
        expect(response).to render_template :new
      end
    end

    context "post valid email" do
      it "redirect_to root etc." do
        expect{
          post :create, params:{password_reset:{email:user.email}}
          user.reload
        }.to change(user, :reset_digest)
        expect(flash[:info]).to be_present
        expect(response).to redirect_to root_url
      end
    end
  end

  describe "GET #edit" do
    render_views
    let(:user) { FactoryBot.create(:user)}

    context "get edit by invalid email" do
      before do
        get :edit, params:{reset_token:user.reset_token, email:""}
      end

      it "redirect to root" do
        expect(response).to redirect_to root_url
      end
    end

    context "get edit by invalid user" do
      before do
        user.toggle!(:activated)
        get :edit, params:{reset_token:user.reset_token, email:user.email}
      end

      it "redirect to root" do
        expect(response).to redirect_to root_url
      end
    end

    context "get edit by valid email and invalid token" do
      before do
        get :edit, params:{reset_token:"wrong token",email:user.email}
      end

      it "redirect to root" do
        expect(response).to redirect_to root_url
      end
    end

    context "get edit by valid email and token" do
      before do
        post :create, params:{password_reset:{email:user.email}} 
        valid_user = assigns(:user)    
        get :edit, params:{reset_token:valid_user.reset_token,email:valid_user.email}
      end

      it "render edit and input email by hidden tag" do
        expect(response).to render_template :edit
        expect(response.body).to have_css('input', visible: false)
      end
    end
  end

  describe "PATCH #update" do
    render_views
    let(:user) { FactoryBot.create(:user)}
    # context "patch invalid password and password_confimation" do
    #   render_views
    #   it "flash error_explanation" do
    #     post :create, params:{password_reset:{email:user.email}} 
    #     valid_user = assigns(:user)
    #     patch :update, params:{reset_token:valid_user.reset_token,email:valid_user.email,
    #                           password_reset:{password: "foobaz",
    #                                           password_confimation:"foofas"}
    #                           }
    #     expect(response.body).to have_css('div',id: "error_explanation")
    #   end
    # end

    context "patch empty password and password_confimation" do
      before do
        post :create, params:{password_reset:{email:user.email}} 
        valid_user = assigns(:user)
        patch :update, params:{reset_token:valid_user.reset_token,email:valid_user.email,
                              password_reset:{password: "",
                                              password_confimation:""}
                              }
      end

      it "flash error_explanation" do
        expect(response.body).to have_css('div',id: "error_explanation")
      end
    end

    context "patch valid password and password_confimation" do
      before do
        post :create, params:{password_reset:{email:user.email}} 
        valid_user = assigns(:user)
        patch :update, params:{reset_token:valid_user.reset_token,email:valid_user.email,
                              password_reset:{password: "foobaz",
                                              password_confimation:"foobaz"}
                              }
      end

      it "flash info and login" do
        valid_user = assigns(:user)

        expect(logged_in?).to eq true
        expect(valid_user.reset_digest).to be_falsey
        expect(flash[:info]).to be_present
        expect(response).to redirect_to valid_user
      end
    end
  end

  describe "expired tokenのテスト" do
    let(:user) { FactoryBot.create(:user)}

    context "the token expired" do
      before do
        get :new
        post :create,params:{password_reset:{email: user.email}}
        valid_user = assigns(:user)
        valid_user.update_attribute(:reset_sent_at,3.hours.ago)
        patch :update, params:{reset_token:valid_user.reset_token,email:valid_user.email,
                              password_reset:{password: "foobar",
                                              password_confimation: "fooobar"}
                              }

      end

      it "redirect to password reset new and show expired message" do
        expect(response).to redirect_to new_password_reset_url
        expect(flash[:danger]).to include "expired"
        expect(flash[:danger]).to be_present
      end
    end
  end
end
