require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do
	context "when not logged in" do
		before do
			FactoryBot.create(:active_relationship_user)
		end

		it "user cant create" do
			expect{
				post :create
			}.not_to change{Relationship.count}
		end

		it "user cant destroy" do
			expect{
				delete :destroy, params:{id: Relationship.first.followed_id}
			}.not_to change{Relationship.count}
		end
	end

	context "when logged in" do
		let(:user) { FactoryBot.create(:user) }
		let(:other) { FactoryBot.create(:other_user) }

		before do
			log_in_as(user)
		end

		it "user should follow an other_user the standard way" do
			expect{
				post :create, params:{followed_id: other.id}
			}.to change{Relationship.count}.by(1)
		end

		it "user should follow an other_user with Ajax" do
			expect{
				post :create, xhr: true, params:{followed_id: other.id}
			}.to change{Relationship.count}.by(1)
		end

		it "user should unfollow an other_user the standard way" do
			user.follow(other)
			relationship = user.active_relationships.find_by(followed_id: other.id)
			expect{
				delete :destroy, params:{id: relationship.id}
			}.to change{Relationship.count}.by(-1)
		end

		it "user should unfollow an other_user with Ajax" do
			user.follow(other)
			relationship = user.active_relationships.find_by(followed_id: other.id)
			expect{
				delete :destroy, xhr: true, params:{id: relationship.id}
			}.to change{Relationship.count}.by(-1)
		end
	end
end
