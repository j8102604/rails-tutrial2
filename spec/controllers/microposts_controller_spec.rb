require 'rails_helper'

RSpec.describe MicropostsController, type: :controller do
	describe "POST #create" do
		render_views

		context "not logged in" do
			it "should redirect create when not logged in" do
				expect{
					post :create, params: {micropost: {content:"test"} }
				}.not_to change{Micropost.count}
				expect(response).to redirect_to login_url
			end
		end

		context "file upload test" do
#		let(:image_path) { File.join(Rails.root,'spec/fixtures/for_test.png') }
#		let(:image) { Rack::Test::UploadedFile.new(image_path)}
		let(:user) { FactoryBot.create(:user) }

			before do
				log_in_as(user)
			end

			it "have picture uploader" do
				content = "This micropost really ties the room together"
				picture = fixture_file_upload('spec/fixtures/for_test.png','image/png')
#				expect(response.body).to have_css('input[type="file"]')
				expect{
					post :create, params:{micropost:
																	{ content: content,
																		picture: picture } }
				}.to change{Micropost.count}.by(1)
				micropost = assigns(:micropost)
				expect(micropost.picture?).to eq true
			end
		end
	end

	describe "DELETE #destroy" do		
		context "not logged in" do
			it "should redirect destroy when not logged in" do
					micropost = FactoryBot.create(:micropost)
					expect{
						delete :destroy, params:{id: micropost.id}
					}.not_to change{Micropost.count}
					expect(response).to redirect_to login_url
			end
		end

		context "other user destroy the post" do
			let(:other_user) { FactoryBot.create(:other_user) }

			before do
				log_in_as(other_user)
			end

			it "redirect to previous page" do
				micropost = FactoryBot.create(:micropost)
				expect{
					delete :destroy, params:{id: micropost.id}
				}.not_to change{Micropost.count}
				expect(response).to redirect_to root_url
			end
		end
	end


end
