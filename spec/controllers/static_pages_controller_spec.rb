require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  render_views

  describe "GET #home" do
    before do
      get :home
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "title is Home" do
      expect(response.body).to include 'Home'
    end
  end

  describe "GET #help" do
    before do
      get :help
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "title is Help" do
      expect(response.body).to include 'Help'
    end
  end

  describe "GET #about" do
    before do
      get :about
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "title is About" do
      expect(response.body).to include 'About'
    end
  end

  describe "GET #contact" do
    before do
      get :contact
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "title is Contact" do
      expect(response.body).to include 'Contact'
    end
  end
end