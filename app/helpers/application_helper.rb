module ApplicationHelper
	def full_title(page_title = '' )
		base_title = "Ruby on Rails Tutorial Sample App"
		if page_title.empty?
			base_title
		else
			page_title + "|" + base_title
		end
	end

	def is_logged_in?
		!session[:user_id].nil?
	end

	def log_in_as(user)
		session[:user_id] = user.id
	end

	def log_in_as2(user, password: 'password', remember_me: '1')
    post :create , params: { session: { email: user.email,
                                    password: password,
                                    remember_me: remember_me } }
  end
end
